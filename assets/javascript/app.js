//game start variable
let shouldStartGame = false;
let hasPlayerWon = false;

//array of starting words
const startingWords = [
        "abruptly",
        "absurd",
        "abyss",
        "affix",
        "askew",
        "avenue",
        "awkward",
        "axiom"
      ];
// Starting Guess Attempts
let startingGuessAttempts = 5;

// Total Wins
let totalWins = 0;

//chosen word
let chosenWord = "";

let correctKeys = [];

//html vars
const blankSpaceContainer = document.getElementById("word-blanks");
const guessedLetters = document.getElementById("guessed-letters");
const winsContainer = document.getElementById("wins");
const attemptsContainer = document.getElementById("attempts");
const startScreenContainer = document.getElementById("starting-screen");


//add event listner for keyup
document.addEventListener('keyup', (event) => {
  let keyPressed = event.key.toLowerCase();
  if(!shouldStartGame){
    //set player has won to false
    hasPlayerWon = false;
    //clear start screen
    startScreenContainer.innerHTML = "";
    attemptsContainer.innerHTML = startingGuessAttempts;
    //choose random word
    const randomWord = startingWords[Math.floor(Math.random() * startingWords.length)];
    //Put the blank spaces for each letter of the random word on the page

      //create blank html placeholder var
      let blankHtml = "";

      //create randomWordHtmlArray then loop through all the letters and add the strings to blank html var
      const randomHtmlArrary = randomWord.split("");

      //add blankhtml to the blankSpaceContainer
      randomHtmlArrary.forEach(letter => {
        let classes = [letter, "blank"];
        let blankSpaceEl = document.createElement('li');
        blankSpaceEl.classList.add(...classes);
        blankSpaceEl.innerHTML = '___'
        blankSpaceContainer.appendChild(blankSpaceEl);
      });

      chosenWord = randomWord;
  }

  //if game has started
  if(shouldStartGame) {
    //check if pressed key equals any of our random word letters
    if (chosenWord.split("").includes(keyPressed)){
      //select all blank space elements that match our pressed key
      let correctLetterSpaces = document.querySelectorAll(`.${keyPressed}`);
      //set the inner html of the blank space to the correct letter
      correctLetterSpaces.forEach(el => {
        el.innerHTML = keyPressed;
      });
      //add the key to correct keys array
      correctKeys.push(keyPressed);
    // else the letter was not equal to the key press and is wrong
    } else {
      //create the li for the wrong letter el
      let classes = ["guessed", "wrong"]
      let wrongGuessEl = document.createElement('li')
      wrongGuessEl.classList.add(...classes)
      wrongGuessEl.innerHTML = keyPressed
      // add the wrong letter el to the page
      guessedLetters.appendChild(wrongGuessEl);
      --startingGuessAttempts
      attemptsContainer.innerHTML = startingGuessAttempts;


    }

    //check if the user has won
    let sortedChosenWord = chosenWord.split("").filter( (elem,index,self) => {
      return index == self.indexOf(elem);
    }).sort();
    let sortedCorrectKeys = correctKeys.filter((elem,index,self) => {
      return index == self.indexOf(elem);
    }).sort();

    // check if the player has one by seeing if the length of the arrays are equal
    if(sortedChosenWord.length == sortedCorrectKeys.length){
      blankSpaceContainer.innerHTML = "";
      guessedLetters.innerHTML = "";
      startScreenContainer.innerHTML = `Congrats! you won! the word was: ${chosenWord} press any key to play again`
      chosenWord = "";
      correctKeys = [];
      hasPlayerWon = true;
      shouldStartGame = false;
    }




  }
  if(hasPlayerWon != true){
    shouldStartGame = true;
  }
  //player lost
  if(startingGuessAttempts == 0){
    shouldStartGame = false;
    console.log("lost game, click to start again");
    //reset guess Attempts
    startingGuessAttempts = 5;
    blankSpaceContainer.innerHTML = "";
    guessedLetters.innerHTML = "";
    shouldStartGame = false;
    startScreenContainer.innerHTML = "sorry! You lost, press any key to try again"
    chosenWord = "";
    correctKeys = [];
  }
});




/* Problems To fix
1- Lots of repeated code. Break into functions to dry up
2- flow of program is confusing. How can we make the flow more logical
